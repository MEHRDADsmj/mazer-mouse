#include "Board.h"
#define CHANCE_EXIT_ONE_IN 100;
#define CHANCE_PATH_ONE_IN 2;


Board::Board(int width, int height)
{
	this->width = width;
	this->height = height;
	for (int coorY = 0; coorY < height; coorY++)
	{
		boardGrid.push_back(std::vector<Square> {});
	}
	for (int coorY = 0; coorY < height; coorY++)
	{
		for (int coorX = 0; coorX < width; coorX++)
		{
			Square initSquare;
			initSquare.SetXY(coorX, coorY);
			boardGrid[coorY].push_back(initSquare);
		}
	}

	//Setting up start
	boardGrid[0][0].SetIsWall(false);
	boardGrid[0][0].SetIsExit(false);
	boardGrid[0][0].SetSymbol('S');
	boardGrid[0][0].SetDistanceFromStart(0);
	currentSquare.AssignFrom(boardGrid[0][0]);
	currentX = 0;
	currentY = 0;

	GenerateBoard();
}

Square Board::GenerateRandomSquare()
{
	Square newSquare;
	int randomForBeingWall = rand() % CHANCE_PATH_ONE_IN;
	if (randomForBeingWall < 1)
	{
		newSquare.SetIsWall(false);
		newSquare.SetSymbol(' ');
	}
	else
	{
		newSquare.SetIsWall(true);
		newSquare.SetSymbol('#');
	}

	return newSquare;
}

void Board::GenerateRestOfBoard()
{
	for (int coorY = 0; coorY < height; coorY++)
	{
		for (int coorX = 0; coorX < width; coorX++)
		{
			if (boardGrid[coorY][coorX].GetSymbol() == 'N')
			{
				Square newSquare = GenerateRandomSquare();
				boardGrid[coorY][coorX].SetIsWall(newSquare.GetIsWall());
				boardGrid[coorY][coorX].SetIsExit(newSquare.GetIsExit());
				boardGrid[coorY][coorX].SetSymbol(newSquare.GetSymbol());
			}
		}
	}
}

void Board::GenerateBoard()
{
	GeneratePath();
	GenerateRestOfBoard();
}

void Board::GeneratePath()
{
	for (;;)
	{
		int direction = rand() % 4;

		switch (direction)
		{
			case 0:
				if (MoveUp() && GetSquareAt(currentX, currentY).GetSymbol() == 'N')
				{
					Square& squareToSet = GetSquareAt(currentX, currentY);
					int randomForBeingExit = rand() % CHANCE_EXIT_ONE_IN;
					//Is it the exit?
					if (randomForBeingExit < 1)
					{
						squareToSet.SetIsExit(true);
						squareToSet.SetSymbol('E');
						ExitSquare.AssignFrom(squareToSet);
						return;
					}
					//So it is a path!
					else
					{
						squareToSet.SetIsWall(false);
						squareToSet.SetSymbol(' ');
					}
				}
				else
					continue;
				break;
			case 1:
				if (MoveRight() && GetSquareAt(currentX, currentY).GetSymbol() == 'N')
				{
					Square& squareToSet = GetSquareAt(currentX, currentY);
					int randomForBeingExit = rand() % CHANCE_EXIT_ONE_IN;
					//Is it the exit?
					if (randomForBeingExit < 1)
					{
						squareToSet.SetIsExit(true);
						squareToSet.SetSymbol('E');
						ExitSquare.AssignFrom(squareToSet);
						return;
					}
					//So it is a path!
					else
					{
						squareToSet.SetIsWall(false);
						squareToSet.SetSymbol(' ');
					}
				}
				else
					continue;
				break;
			case 2:
				if (MoveLeft() && GetSquareAt(currentX, currentY).GetSymbol() == 'N')
				{
					Square& squareToSet = GetSquareAt(currentX, currentY);
					int randomForBeingExit = rand() % CHANCE_EXIT_ONE_IN;
					//Is it the exit?
					if (randomForBeingExit < 1)
					{
						squareToSet.SetIsExit(true);
						squareToSet.SetSymbol('E');
						ExitSquare.AssignFrom(squareToSet);
						return;
					}
					//So it is a path!
					else
					{
						squareToSet.SetIsWall(false);
						squareToSet.SetSymbol(' ');
					}
				}
				else
					continue;
				break;
			case 3:
				if (MoveDown() && GetSquareAt(currentX, currentY).GetSymbol() == 'N')
				{
					Square& squareToSet = GetSquareAt(currentX, currentY);
					int randomForBeingExit = rand() % CHANCE_EXIT_ONE_IN;
					//Is it the exit?
					if (randomForBeingExit < 1)
					{
						squareToSet.SetIsExit(true);
						squareToSet.SetSymbol('E');
						ExitSquare.AssignFrom(squareToSet);
						return;

					}
					//So it is a path!
					else
					{
						squareToSet.SetIsWall(false);
						squareToSet.SetSymbol(' ');
					}
				}
				else
					continue;
				break;
		}
	}
}

void Board::FindPath()
{
	IndexSquares();
	std::vector<Square*> route = TrackBackFromExit();
	for (auto square : route)
	{
		square->SetSymbol('.');
	}
}

//TODO: CHECK FOR POSSIBLE BUGS
void Board::IndexSquares()
{
	currentSquare.AssignFrom(boardGrid[0][0]);
	currentX = 0;
	currentY = 0;
	for (int distance = 0; ; distance++)
	{
		std::vector<Square*> squaresWithIndex = GetSquaresWithDistance(distance);
		for (auto tempSquare : squaresWithIndex)
		{
			currentSquare.AssignFrom(*tempSquare);
			tempSquare->GetXY(currentX, currentY);
			std::vector<Square*> nearbySquares = GetNearbySquares();
			for (auto square : nearbySquares)
			{
				if (square->GetDistanceFromStart() == -1 && !square->GetIsWall())
				{
					square->SetDistanceFromStart(distance + 1);
					if (square->GetIsExit())
					{
						ExitSquare.AssignFrom(*square);
						return;
					}
				}
			}
		}
	}
}

std::vector<Square*> Board::GetSquaresWithDistance(int distance)
{
	std::vector<Square*> resultVector;
	for (int coorY = 0; coorY < height; coorY++)
	{
		for (int coorX = 0; coorX < width; coorX++)
		{
			if (boardGrid[coorY][coorX].GetDistanceFromStart() == distance)
			{
				resultVector.push_back(&boardGrid[coorY][coorX]);
			}
		}
	}
	return resultVector;
}

std::vector<Square*> Board::GetNearbySquares()
{
	std::vector<Square*> nearbySquares;
	if (MoveUp())
	{
		nearbySquares.push_back(&boardGrid[currentY][currentX]);
		MoveDown();
	}
	if (MoveRight())
	{
		nearbySquares.push_back(&boardGrid[currentY][currentX]);
		MoveLeft();
	}
	if (MoveLeft())
	{
		nearbySquares.push_back(&boardGrid[currentY][currentX]);
		MoveRight();
	}
	if (MoveDown())
	{
		nearbySquares.push_back(&boardGrid[currentY][currentX]);
		MoveUp();
	}
	return nearbySquares;
}

std::vector<Square*> Board::TrackBackFromExit()
{
	currentSquare.AssignFrom(ExitSquare);
	ExitSquare.GetXY(currentX, currentY);
	std::vector<Square*> route;
	for (; currentSquare.GetDistanceFromStart() != 1;)
	{
		std::vector<Square*> nearby = GetNearbySquares();
		for (auto square : nearby)
		{
			if (square->GetDistanceFromStart() < currentSquare.GetDistanceFromStart() && square->GetDistanceFromStart() != -1)
			{
				route.push_back(square);
				currentSquare.AssignFrom(*square);
				square->GetXY(currentX, currentY);
			}
		}
	}
	return route;
}

Square& Board::GetSquareAt(int width, int height)
{
	if (width < this->width && height < this->height && width >= 0 && height >= 0)
	{
		return boardGrid[height][width];
	}
	Square nullSquare;
	return nullSquare;
}

void Board::Print()
{
	system("cls");
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			std::cout << boardGrid[y][x].GetSymbol() << " ";
		}
		std::cout << std::endl;
	}
}

bool Board::MoveUp()
{
	if (currentX < width && currentY - 1 < height && currentX >= 0 && currentY - 1 >= 0)
	{
		MoveCurrentSquare(0, -1);
		return true;
	}
	return false;
}

bool Board::MoveRight()
{
	if (currentX + 1 < width && currentY < height && currentX + 1 >= 0 && currentY >= 0)
	{
		MoveCurrentSquare(1, 0);
		return true;
	}
	return false;
}

bool Board::MoveLeft()
{
	if (currentX - 1 < width && currentY < height && currentX - 1 >= 0 && currentY >= 0)
	{
		MoveCurrentSquare(-1, 0);
		return true;
	}
	return false;

}

bool Board::MoveDown()
{
	if (currentX < width && currentY + 1 < height && currentX >= 0 && currentY + 1 >= 0)
	{
		MoveCurrentSquare(0, 1);
		return true;
	}
	return false;
}

void Board::MoveCurrentSquare(short deltaX, short deltaY)
{
	currentX += deltaX;
	currentY += deltaY;
	currentSquare = boardGrid[currentY][currentX];
}
