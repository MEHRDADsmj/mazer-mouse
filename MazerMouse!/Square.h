#pragma once

class Square
{
public:
	Square() = default;
	~Square() = default;

	void SetIsWall(bool value);
	void SetIsExit(bool value);
	void SetSymbol(char suitableSymbol);
	void SetDistanceFromStart(int distance);
	void SetXY(int X, int Y);

	bool GetIsWall() const;
	bool GetIsExit() const;
	char GetSymbol() const;
	int GetDistanceFromStart() const;
	void GetXY(int& X, int& Y);

	void AssignFrom(Square& otherSquare);

private:
	bool bIsWall = false;
	bool bIsExit = false;
	char symbol = 'N';
	int distanceFromStart = -1;
	int x, y;
};