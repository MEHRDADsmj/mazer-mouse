#pragma once

#include <iostream>
#include <vector>
#include "Square.h"
#include <cstdlib>


class Board
{
public:
	Board() = delete;
	Board(int width, int height);
	~Board() = default;

	void Print();
	void FindPath();

private:
	int width;
	int height;
	int currentX = 0;
	int currentY = 0;
	std::vector<std::vector<Square>> boardGrid;
	Square currentSquare;
	Square ExitSquare;
	
	Square& GetSquareAt(int width, int height);
	
	//Movement
	void MoveCurrentSquare(short deltaX, short deltaY);
	bool MoveUp();
	bool MoveRight();
	bool MoveLeft();
	bool MoveDown();
	
	//Generation
	Square GenerateRandomSquare();
	void GenerateRestOfBoard();
	void GenerateBoard();
	void GeneratePath();
	
	//Path finding
	void IndexSquares();
	std::vector<Square*> GetSquaresWithDistance(int distance);
	std::vector<Square*> GetNearbySquares();
	std::vector<Square*> TrackBackFromExit();
};

