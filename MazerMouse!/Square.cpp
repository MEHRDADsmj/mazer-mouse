#include "Square.h"



void Square::SetIsWall(bool value)
{
	bIsWall = value;
}

void Square::SetIsExit(bool value)
{
	bIsExit = value;
}

void Square::SetSymbol(char suitableSymbol)
{
	symbol = suitableSymbol;
}

void Square::SetDistanceFromStart(int distance)
{
	distanceFromStart = distance;
}

void Square::SetXY(int X, int Y)
{
	x = X;
	y = Y;
}

bool Square::GetIsWall() const
{
	return bIsWall;
}

bool Square::GetIsExit() const
{
	return bIsExit;
}

char Square::GetSymbol() const
{
	return symbol;
}

int Square::GetDistanceFromStart() const
{
	return distanceFromStart;
}

void Square::GetXY(int& X, int& Y)
{
	X = x;
	Y = y;
}

void Square::AssignFrom(Square& otherSquare)
{
	this->bIsExit = otherSquare.GetIsExit();
	this->bIsWall = otherSquare.GetIsWall();
	this->symbol = otherSquare.GetSymbol();
	otherSquare.GetXY(this->x, this->y);
	this->distanceFromStart = otherSquare.GetDistanceFromStart();
}
